const cacheName = 'v3';

class DB {
  static transaction(action) {
    let openReq = indexedDB.open("flip_db", "1");

    openReq.onerror = function (event) {
      console.log('failed to initiate indexedDB');
    }

    openReq.onsuccess = function (event) {
      let db = event.target.result;
      action(db);
    }
  }

  static async getAll(key) {
    return await this.getAllAsync(key);
  }

  static getAllAsync(key) {
    return new Promise((resolve) => {
      this.transaction(db => {
        let getReq = db.transaction(key, "readwrite").objectStore(key).getAll();
        getReq.onsuccess = event => resolve(event.target.result);
      })
    });
  }
}

// Call Install Event
self.addEventListener('install', (e) => {
  console.log('Service Worker: Installed');
})

// Call Activate Event

self.addEventListener('activate', e => {
  console.log('Service Worker: Activated');

  // Remove unwanted caches
  e.waitUntil(
    caches.keys().then(cacheNames => {
      return Promise.all(
        cacheNames.map(cache => {
          if (cache !== cacheName) {
            console.log("Service Worker: Clearing Old Cache:")
            return caches.delete(cache);
          }
        })
      )
    })
  )
});

// Call Fetch Event
self.addEventListener('fetch', e => {
  console.log('Service Worker: Fetching');
  e.respondWith(
    fetch(e.request)
      .then(res => {
        // Make copy/clone of response
        const resClone = res.clone();
        // Open cache
        caches
          .open(cacheName)
          .then(cache => {
            // Add response to cache
            if (e.request.method === "GET" && e.request.url.startsWith("http")) cache.put(e.request, resClone);
          })
        return res;
      }).catch(err => caches.match(e.request).then(res => res))
  )
});

self.addEventListener('push', async function (event) {
  console.log(`[Service Worker] Push had this data: "${event.data.text()}"`);
  const settings = await DB.getAll('settings');

  if (!settings[0].pushPermission) return;

  const payload = event.data.json();
  const cards = await DB.getAll('cards');
  const targetCard = cards.reduce((targetCard, indexCard) => {
    if (targetCard && "score" in targetCard && "score" in indexCard) {
      if (targetCard.score < indexCard.score) return targetCard;
      else if (targetCard.score > indexCard.score) return indexCard;
      return Math.random(0, 1) > 0.5 ? targetCard : indexCard;
    }
    return Math.random(0, 1) > 0.5 ? targetCard : indexCard;
  }, null);
  if (targetCard) self.registration.showNotification(targetCard.front, { body: "It's time for daily quizs!" })
  // event.waitUntil(self.registration.showNotification(targetCard.front, {body: "It's time for daily quizs!"}));
});