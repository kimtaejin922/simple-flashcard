export function register() {
  (() => {
    if ('serviceWorker' in navigator) {
      window.addEventListener('load', () => {
        navigator.serviceWorker.register('sw_cached_site.js');
      })

      navigator.serviceWorker.ready
        .then(registration => {
          console.log('Service Worker: Registered');
          if (Notification.permission !== 'granted') return registration;

          const subscribeOptions = {
            userVisibleOnly: true,
            applicationServerKey: urlBase64ToUint8Array(
              'BEl62iUYgUivxIkv69yViEuiBIa-Ib9-SkvMeAtA3LFgDzkrxZJjSgSnfckjBJuBkr3qBUYIHBQFLXYp5Nksh8U'
            )
          };

          return registration.pushManager.subscribe(subscribeOptions);
        })
        .catch(err => console.log(`Service Worker: Error: ${err}`))
        .then(function (pushSubscription) {
          if (pushSubscription) sendPushSubscriptionToServer(pushSubscription);
          return pushSubscription;
        });
    }

    function sendPushSubscriptionToServer(pushSubscription) {
      console.log('Received PushSubscription: ', JSON.stringify(pushSubscription));
      postData(process.env.REACT_APP_PUSH_SUBSCRIPTION_URL, pushSubscription)
    }

    function postData(url = '', data = {}) {
      return fetch(url, {
        method: 'POST',
        mode: 'cors',
        body: JSON.stringify(data), // body data type must match "Content-Type" header
      }).then(response => response.json())
        .then(response => console.log('Success:', JSON.stringify(response)))
        .catch(error => console.error('Error:', error));
    }

    /**
     * urlBase64ToUint8Array
     * 
     * @param {string} base64String a public vavid key
     */
    function urlBase64ToUint8Array(base64String) {
      var padding = '='.repeat((4 - base64String.length % 4) % 4);
      var base64 = (base64String + padding)
        .replace(/\-/g, '+')
        .replace(/_/g, '/');

      var rawData = window.atob(base64);
      var outputArray = new Uint8Array(rawData.length);

      for (var i = 0; i < rawData.length; ++i) {
        outputArray[i] = rawData.charCodeAt(i);
      }
      return outputArray;
    }
  })();
}