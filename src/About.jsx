import React, {useEffect} from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import Typography from '@material-ui/core/Typography';
import TextData from './TextData';

const useStyles = makeStyles(theme => ({
  heading: {
    padding: theme.spacing(4, 0, 4),
  },
  root: {
    paddingLeft: '16px',
    paddingRight: '16px',
  },
}));

export default function About(props) {
  const classes = useStyles();

  useEffect(() => {
    props.pageView('about');
  }, [])

  return (
    <div className={classes.root}>
      <Container maxWidth="sm">
        <Typography className={classes.heading} component="h3" variant="h3" align="center" color="textPrimary" gutterBottom>
          {TextData.text.about}
        </Typography>
        <Typography component="h6" variant="h6" align="left" color="textPrimary" gutterBottom>
          Flip: Simple Flashcard
        </Typography>
        <Typography component="h6" variant="h6" align="left" color="textPrimary" gutterBottom>
          Copyright © 2020 Taejin Kim.
        </Typography>
        <Typography component="h6" variant="h6" align="left" color="textPrimary" gutterBottom>
          oldmanstudio922@gmail.com
        </Typography>
      </Container>
    </div>
  )
}