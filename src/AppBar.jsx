import React, { useState } from 'react';
import MuiAppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import { makeStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import SwipeableDrawer from '@material-ui/core/SwipeableDrawer';
import Link from '@material-ui/core/Link';
import Divider from '@material-ui/core/Divider';
import FontDownloadIcon from '@material-ui/icons/FontDownload';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import FolderIcon from '@material-ui/icons/Folder';
import ListAltIcon from '@material-ui/icons/ListAlt';
import QueueIcon from '@material-ui/icons/Queue';
import FilterNoneIcon from '@material-ui/icons/FilterNone';
import CreateNewFolderIcon from '@material-ui/icons/CreateNewFolder';
import { Link as RouterLink } from "react-router-dom";
import SettingsIcon from '@material-ui/icons/Settings';
import AccountBoxIcon from '@material-ui/icons/AccountBox';
import TextData from './TextData';

const useStyles = makeStyles({
  list: {
    width: 250,
  },
  fullList: {
    width: 'auto',
  },
  title: {
    flexGrow: 1
  }
});

export default function AppBar(props) {
  const [state, setState] = useState({
    top: false,
    left: false,
    bottom: false,
    right: false,
  });
  const classes = useStyles();

  const toggleDrawer = (side, open) => event => {
    if (event && event.type === 'keydown' && (event.key === 'Tab' || event.key === 'Shift')) {
      return;
    }

    setState({ ...state, [side]: open });
  };

  const sideList = (side, props) => (
    <div
      className={classes.list}
      role="presentation"
      onClick={toggleDrawer(side, false)}
      onKeyDown={toggleDrawer(side, false)}
    >
      <List>
        <ListItem>
          <ListItemIcon>
            <FontDownloadIcon />
          </ListItemIcon>
          <Typography variant="h5" component="h4" align="left" gutterBottom={false}>
            {TextData.text.card}
          </Typography>
        </ListItem>
        <ListItem button>
          <ListAltIcon />
          <Link variant="h6" underline="none" component={RouterLink} to={`${process.env.REACT_APP_BASE_PATH}/card`} style={{ paddingLeft: '20px' }}>
            {TextData.text.showCardList}
          </Link>
        </ListItem>
        <ListItem button disabled={!props.filteredCards.length}>
          <FilterNoneIcon></FilterNoneIcon>
          <Link variant="h6" underline="none" component={RouterLink} to={`${process.env.REACT_APP_BASE_PATH}/flashcard`} style={{ paddingLeft: '20px' }}>
            {TextData.text.showAsFlashcard}
          </Link>
        </ListItem>
        <ListItem button >
          <QueueIcon></QueueIcon>
          <Link variant="h6" underline="none" component={RouterLink} to={`${process.env.REACT_APP_BASE_PATH}/card/create`} style={{ paddingLeft: '20px' }}>
            {TextData.text.addNewCard}
          </Link>
        </ListItem>
        <Divider />
        <ListItem>
          <ListItemIcon>
            <FolderIcon />
          </ListItemIcon>
          <Typography variant="h5" component="h4" align="left" gutterBottom={false}>
            {TextData.text.cardGroup}
          </Typography>
        </ListItem>
        <ListItem>
          <ListAltIcon />
          <Link variant="h6" underline="none" component={RouterLink} to={`${process.env.REACT_APP_BASE_PATH}/group`} style={{ paddingLeft: '20px' }}>
            {TextData.text.showGroupList}
          </Link>
        </ListItem>
        <ListItem button >
          <CreateNewFolderIcon></CreateNewFolderIcon>
          <Link variant="h6" underline="none" component={RouterLink} to={`${process.env.REACT_APP_BASE_PATH}/group/create`} style={{ paddingLeft: '20px' }}>
            {TextData.text.addNewCardGroup}
          </Link>
        </ListItem>
        <Divider />
        <ListItem button >
          <AccountBoxIcon></AccountBoxIcon>
          <Link variant="h6" underline="none" component={RouterLink} to={`${process.env.REACT_APP_BASE_PATH}/about`} style={{ paddingLeft: '20px' }}>
          {TextData.text.about}
          </Link>
        </ListItem>
      </List>
    </div>
  );

  return (
    <MuiAppBar position="static">
      <Toolbar>
        <IconButton edge="start" color="inherit" aria-label="menu" onClick={toggleDrawer('left', true)}>
          <MenuIcon />
        </IconButton>
        <SwipeableDrawer
          open={state.left}
          onClose={toggleDrawer('left', false)}
          onOpen={toggleDrawer('left', true)}
        >
          {sideList('left', props)}
        </SwipeableDrawer>
        <Typography variant="h6" className={classes.title}>
          Flip
        </Typography>
        <IconButton aria-controls="menu-appbar" component={RouterLink} to={`${process.env.REACT_APP_BASE_PATH}/settings`}>
          <SettingsIcon style={{ color: 'white' }} />
        </IconButton>
      </Toolbar>
    </MuiAppBar>
  );
}