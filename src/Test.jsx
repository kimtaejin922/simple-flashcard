import React, { useState, useEffect } from 'react';
import FlipCard from './FlipCard';
import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert from '@material-ui/lab/Alert';
import Result from './Result';
import Typography from '@material-ui/core/Typography';
import TextData from './TextData';

function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}

const useStyles = makeStyles(theme => ({
  root: {
    '& > *': {
      margin: theme.spacing(2),
    },
    textAlign: 'center'
  },
  o: {
    color: 'white',
    backgroundColor: '#09A603',
    "&:hover": {
      background: "#09A603"
    },
  },
  x: {
    color: 'white',
    backgroundColor: '#F24B59',
    "&:hover": {
      background: "#F24B59"
    },
  }
}));

function Test(props) {
  const [cardSet, setCardSet] = useState([]);
  const [index, setIndex] = useState(0);
  const [fromLeft, setFromLeft] = useState(false);
  const [showFeedBack, setShowFeedBack] = useState(false);
  const [alertType, setAlertType] = useState('success');
  const [open, setOpen] = useState(false);
  const [showResult, setShowResult] = useState(false);
  const [correctList, setCorrectList] = useState([]);
  const [incorrectList, setIncorrectList] = useState([]);
  const classes = useStyles();

  useEffect(() => {
    props.pageView('flashcard');
  }, [])

  useEffect(() => {
    const cardGroup = getCardGroup();
    let cards = [];
    if (props.cards.length) {
      if (props.settings.shuffleCards === true) {
        cards = getShuffledCards(cardGroup);
      } else {
        cards = cardGroup.slice(0, Math.min(cardGroup.length, props.settings.maxFlashcards))
      }
    }
    setCardSet(cards);
  }, [props.settings]);

  function nextCard() {
    if (index < props.cards.length - 1) {
      setFromLeft(false);
      setIndex(index + 1);
      setShowFeedBack(false);
    }
  }

  function prevCard() {
    if (index > 0) {
      setFromLeft(true);
      setIndex(index - 1);
      setShowFeedBack(false);
    }
  }

  function feedBack(isCorrect) {
    let card = cardSet[index];
    let score = 0;

    if ('score' in card) {
      score = isCorrect ? card.score + 1 : card.score - 1;
      if (score < 0) score = 0;
      if (score > 5) score = 5;
    } else {
      score = isCorrect ? 1 : 0;
    }
    let newCards = [];
    for (let i = 0; i < props.cards.length; i++) {
      if (props.cards[i].id === card.id) {
        newCards.push({ ...card, score: score });
      } else {
        newCards.push({ ...props.cards[i] });
      }
    }

    setCorrectList(correctList.filter(c => { return c.id !== card.id }));
    setIncorrectList(incorrectList.filter(c => { return c.id !== card.id }));

    if (isCorrect) {
      let list = correctList;
      list.push(card);
      setCorrectList(list);

      setAlertType('success');
      setOpen(true);
      setTimeout(() => setOpen(false), 1000);
    } else {
      let list = incorrectList;
      list.push(card);
      setIncorrectList(list);

      setAlertType('error');
      setOpen(true);
      setTimeout(() => setOpen(false), 1000);
    }
    props.setCards(newCards);

    if (index === cardSet.length - 1) {
      setTimeout(() => { setShowResult(true) }, 1000);
    }
  }

  function getCardGroup() {
    let cards = [];

    if (props.settings.flashcardGroup === 'All') {
      cards = props.cards;
    } else if (props.settings.flashcardGroup === 'No Group') {
      cards = props.cards.filter(card => {
        return !('groupName' in card)
      })
    } else {
      cards = props.cards.filter(card => {
        return card.groupName === props.settings.flashcardGroup
      });
    }

    return cards;
  }

  function getShuffledCards(cardArray) {
    let cards = [];

    let ids = [];
    cardArray.forEach(card => {
      const weight = card.score ? 6 - card.score : 6;
      [...Array(weight * 2).keys()].forEach(n => { ids.push(card.id); });
    });

    const numOfCards = Math.min(cardArray.length, props.settings.maxFlashcards);
    for (let i = 0; i < numOfCards; i++) {
      const picked = Math.floor(Math.random() * ids.length);
      cards.push({ ...props.cards.filter(card => { return card.id === ids[picked] })[0] });
      ids = ids.filter(id => { return id !== ids[picked] });
    }

    return cards;
  }
  
  return (
    <div>
      {showResult ?
        <>
          <Result pageView={props.pageView} rerender={props.rerender} cardSet={cardSet} correctList={correctList} incorrectList={incorrectList}></Result>
        </>
        :
        <>
          {cardSet.length > 0 ?
            <>
              <div style={{ position: 'fixed', marginTop: '20px', left: '50%', transform: 'translate(-50%, 50%)' }}>
                {index + 1} / {cardSet.length}
              </div>
              <FlipCard settings={props.settings} card={cardSet[index]} cardSet={cardSet} nextCard={nextCard} prevCard={prevCard} index={index} fromLeft={fromLeft} setShowFeedBack={setShowFeedBack} />
              {showFeedBack &&
                <div className={classes.root}>
                  <Button className={classes.o} variant="contained" onClick={e => feedBack(true)} size="large">O</Button>
                  <Button className={classes.x} variant="contained" onClick={e => feedBack(false)} size="large">X</Button>
                </div>
              }
              <Snackbar open={open} anchorOrigin={{ vertical: 'top', horizontal: 'center' }}>
                <Alert severity={alertType} style={{ marginTop: '100px' }}>
                  {alertType === 'success' ? TextData.text.markCorrect : TextData.text.markIncorrect}
                </Alert>
              </Snackbar>
            </>
            :
            <>
              <Typography className={classes.heading} component="h6" variant="h6" align="left" color="textPrimary" gutterBottom style={{padding: '50px 16px 50px 16px'}}>
                {TextData.text.noCards}
              </Typography>
            </>
          }

        </>
      }
    </div>
  );
}

export default Test;
