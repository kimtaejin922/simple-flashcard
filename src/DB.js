class DB {
    static transaction(action) {
        let openReq = indexedDB.open(process.env.REACT_APP_DB_NAME, process.env.REACT_APP_DB_VERSION);

        openReq.onerror = function (event) {
            console.log('failed to initiate indexedDB');
        }

        openReq.onupgradeneeded = function (event) {
            var db = event.target.result;

            const cardsObjectStore = db.createObjectStore("cards", { keyPath: 'id' })
            cardsObjectStore.createIndex("id", "id", { unique: true });
            cardsObjectStore.createIndex("front", "front", { unique: false });
            cardsObjectStore.createIndex("back", "back", { unique: false });
            cardsObjectStore.createIndex("groupName", "groupName", { unique: false });

            const groupsObjectStore = db.createObjectStore("groups", { keyPath: 'id' })
            groupsObjectStore.createIndex("id", "id", { unique: true });
            groupsObjectStore.createIndex("name", "name", { unique: false });

            const settingsObjectStore = db.createObjectStore("settings", { keyPath: 'id' })
            settingsObjectStore.createIndex("id", "id", { unique: true });
            settingsObjectStore.createIndex("flashcardGroup", "flashcardGroup", { unique: false });
            settingsObjectStore.createIndex("shuffleCards", "shuffleCards", { unique: false });
            settingsObjectStore.createIndex("sideToDisplay", "sideToDisplay", { unique: false });
            settingsObjectStore.createIndex("maxFlashcards", "maxFlashcards", { unique: false });

            console.log('DB更新');
        }

        openReq.onsuccess = function (event) {
            let db = event.target.result;
            action(db);
        }
    }

    static put(key, value) {
        this.transaction(db => {
            let putReq = db.transaction(key, "readwrite").objectStore(key).put(value);
            putReq.onsuccess = event => console.log("db updated successfully");
        })
    }

    static delete(key, id) {
        this.transaction(db => {
            let putReq = db.transaction(key, "readwrite").objectStore(key).delete(id);
            putReq.onsuccess = event => console.log("db updated successfully");
        })
    }

    static async getAll(key) {
        return await this.getAllAsync(key);
    }

    static getAllAsync(key) {
        return new Promise((resolve) => {
            this.transaction(db => {
                let getReq = db.transaction(key, "readwrite").objectStore(key).getAll();
                getReq.onsuccess = event => resolve(event.target.result);
            })
        });
    }
}

export default DB;