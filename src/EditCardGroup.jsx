import React, { useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Redirect, useParams, Link as RouterLink } from "react-router-dom";
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import Grid from '@material-ui/core/Grid';
import TextData from './TextData';

const useStyles = makeStyles(theme => ({
  outerContainer: {
    position: 'fixed',
    overflowY: 'scroll',
    height: '100%',
    left: 0,
    right: 0,
  },
  heading: {
    padding: theme.spacing(4, 0, 4),
  },
  root: {
    paddingLeft: '16px',
    paddingRight: '16px',
  }
}));

export default function EditCardGroup(props) {
  const [name, setName] = useState("");
  const [redirect, setRedirect] = useState(null);
  const [errors, setErrors] = useState({});
  const classes = useStyles();
  let { id } = useParams();

  useEffect(() => {
    console.log(props.groups, id);
    const group = props.groups.filter(group => { return group.id === Number(id) })[0];
    setName(group.name);
  }, [props.groups, id])

  useEffect(() => {
    if (name) errors.name = "";
  }, [name])

  useEffect(() => {
    props.pageView('group/edit');
  }, [])

  function editGroup() {
    if (validate()) {
      let group = props.groups.filter(group => { return group.id === Number(id) })[0];
      let otherGroups = props.groups.filter(group => { return group.id != id });

      props.setGroups([...otherGroups, { ...group, name: name }].sort((a, b) => {
        return a.id - b.id;
      }));

      props.setCards([...props.cards.map(card => {
        if (card.groupName == group.name) card.groupName = name;
        return card;
      })]);

      setRedirect(`${process.env.REACT_APP_BASE_PATH}/group`);
    }
  }

  function validate() {
    let err = {}
    let result = true;
    if (!name) {
      result = false
      err.name = "Please input Group Name";
    }
    const nameArray = ["All", "No Group"];
    props.groups.forEach(group => { nameArray.push(group.name) });
    if (nameArray.includes(name)) {
      result = false
      err.name = "Group name already in use";
    }
    setErrors(err);
    return result;
  }

  return (
    <div className={classes.outerContainer}>
      <div className={classes.root}>
        {redirect &&
          <Redirect to={redirect} />
        }
        <Typography className={classes.heading} component="h3" variant="h3" align="center" color="textPrimary" gutterBottom>
          {TextData.text.editCardGroup}
        </Typography>
        <Grid container spacing={3}>
          <Grid item xs={12}>
            <TextField
              name="name"
              variant="outlined"
              error={!!errors.name}
              helperText={errors.name}
              id="name"
              label="Group Name"
              onChange={e => setName(e.target.value)}
              value={name}
              fullWidth
            />
          </Grid>
          <Grid item xs={12}>
            <Button variant="contained" color="primary" fullWidth onClick={editGroup} size="large">{TextData.text.edit}</Button>
          </Grid>
          <Grid item xs={12}>
            <Button variant="outlined" color="default" fullWidth size="large" component={RouterLink} to={`${process.env.REACT_APP_BASE_PATH}/group`}>{TextData.text.cancel}</Button>
          </Grid>
        </Grid>
      </div>
    </div>
  );
}