import React, { useState, useEffect } from 'react';
import {
  Link as RouterLink
} from "react-router-dom";
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Typography from '@material-ui/core/Typography';
import Container from '@material-ui/core/Container';
import Button from '@material-ui/core/Button';
import Fab from '@material-ui/core/Fab';
import AddIcon from '@material-ui/icons/Add';
import EditIcon from '@material-ui/icons/Edit';
import StyleIcon from '@material-ui/icons/Style';
import DeleteIcon from '@material-ui/icons/Delete';
import IconButton from '@material-ui/core/IconButton';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import TextData from './TextData';

const useStyles = makeStyles(theme => ({
  outerContainer: {
    position: 'fixed',
    overflowY: 'scroll',
    height: '100%',
    left: 0,
    right: 0,
  },
  root: {
    width: '100%',
  },
  heading: {
    fontSize: theme.typography.pxToRem(15),
    flexBasis: '33.33%',
    flexShrink: 0,
  },
  secondaryHeading: {
    fontSize: theme.typography.pxToRem(15),
    color: theme.palette.text.secondary,
  },
  heroContent: {
    padding: theme.spacing(4, 0, 4),
    paddingBottom: '200px',
  },
  fab: {
    position: 'fixed',
    bottom: theme.spacing(6),
    right: theme.spacing(2),
  },
  formControl: {
    margin: theme.spacing(1),
    minWidth: 120,
  },
  selectEmpty: {
    marginTop: theme.spacing(2),
  },
}));

export default function CardList(props) {
  const classes = useStyles();
  const [showFront, setShowFront] = useState(true);
  const [showBack, setShowBack] = useState(true);
  const [isDelMode, setIsDelMode] = useState(false);
  const [isEditMode, setIsEditMode] = useState(false);

  useEffect(() => {
    props.pageView('card');
  }, [])

  function deleteCard(card) {
    if (window.confirm("Delete " + card.front + "?")) {
      props.setCards(props.cards.filter(c => c.id !== card.id));
    }
  }

  return (
    <div className={classes.outerContainer}>
      <div className={classes.heroContent}>
        <Container maxWidth="sm">
          <Typography component="h3" variant="h3" align="center" color="textPrimary" gutterBottom>
            {TextData.text.cardList}
          </Typography>
          <div>
            <FormControl className={classes.formControl}>
              <InputLabel id="group-select-label">{TextData.text.cardGroup}</InputLabel>
              <Select
                labelId="group-select-label"
                id="group-select"
                value={props.currentGroup}
                onChange={e => props.setCurrentGroup(e.target.value)}
              >
                <MenuItem value={"All"}>{TextData.text.all}</MenuItem>
                <MenuItem value={"No Group"}>{TextData.text.noGroup}</MenuItem>
                {props.groups.map(group => {
                  return <MenuItem key={group.name} value={group.name}>{group.name}</MenuItem>
                })}
              </Select>
            </FormControl>
            <div style={{ float: "right", top: "25px", position: "relative" }}>
              <IconButton aria-label="add" size="small" component={RouterLink} to={`${process.env.REACT_APP_BASE_PATH}/card/create`}>
                <AddIcon fontSize="small" />
              </IconButton>
              <IconButton style={{ marginLeft: "20px" }} aria-label="edit" size="small" onClick={e => { setIsDelMode(false); setIsEditMode(!isEditMode) }}>
                <EditIcon fontSize="small" color={isEditMode ? "error" : "action"} />
              </IconButton>
              <IconButton style={{ marginLeft: "20px" }} aria-label="delete" size="small" onClick={e => { setIsEditMode(false); setIsDelMode(!isDelMode) }}>
                <DeleteIcon fontSize="small" color={isDelMode ? "error" : "action"} />
              </IconButton>
            </div>
          </div>
          <Table size="small">
            <TableHead>
              <TableRow>
                <TableCell width={isDelMode || isEditMode ? "45%" : "50%"}>
                  <Button onClick={e => setShowFront(!showFront)} color={showFront ? "primary" : "default"}>{TextData.text.front}</Button>
                </TableCell>
                <TableCell width={isDelMode || isEditMode ? "45%" : "50%"}>
                  <Button onClick={e => setShowBack(!showBack)} color={showBack ? "primary" : "default"}>{TextData.text.back}</Button>
                </TableCell>
                {(isDelMode || isEditMode) &&
                  <TableCell width={(isDelMode || isEditMode) ? "10%" : "0%"}></TableCell>
                }
              </TableRow>
            </TableHead>
            <TableBody>
              {props.filteredCards.map(card => (
                <TableRow key={card.id}>
                  <TableCell size="medium">{showFront && card.front}</TableCell>
                  <TableCell size="medium">{showBack && card.back}</TableCell>
                  {isDelMode &&
                    <TableCell size="medium">
                      <IconButton aria-label="delete" size="small" onClick={e => deleteCard(card)}>
                        <DeleteIcon fontSize="small" />
                      </IconButton>
                    </TableCell>
                  }
                  {isEditMode &&
                    <TableCell size="medium">
                      <IconButton aria-label="edit" size="small" component={RouterLink} to={`${process.env.REACT_APP_BASE_PATH}/card/${card.id}`}>
                        <EditIcon fontSize="small" />
                      </IconButton>
                    </TableCell>
                  }
                </TableRow>
              ))}
            </TableBody>
          </Table>
          <Fab color="primary" aria-label="style" className={classes.fab} component={RouterLink} to={`${process.env.REACT_APP_BASE_PATH}/flashcard`}>
            <StyleIcon fontSize="large"/>
          </Fab>
          {/* <Snackbar open={open}>
            <Alert severity="success" style={{ marginBottom: '30px' }}>
              New Card Edited Succesfully
          </Alert>
          </Snackbar> */}
        </Container>
      </div>
    </div>
  );
}
