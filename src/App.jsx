import React, { useState, useEffect } from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect
} from "react-router-dom";
import Test from './Test';
import CardList from './CardList';
import GroupList from './GroupList';
import NewCard from './NewCard';
import EditCard from './EditCard';
import EditCardGroup from './EditCardGroup';
import NewCardGroup from './NewCardGroup';
import sampleVocabs from './SampleVocabs';
import TextData from './TextData';
import AppBar from './AppBar'
import Settings from './Settings'
import About from './About'
import Container from '@material-ui/core/Container';
import DB from './DB';

window.dataLayer = window.dataLayer || [];
function gtag() { window.dataLayer.push(arguments); }

const containerStyle = {
  zIndex: -1,
}

const App = () => {
  const [cards, setCards] = useState([]);
  const [groups, setGroups] = useState([]);
  const [currentGroup, setCurrentGroup] = useState("All");
  const [filteredCards, setFilteredCards] = useState([]);
  const [settings, setSettings] = useState();
  let [renderKey, rerender] = useState();

  useEffect(() => {
    loadData();
    loadSettings();
    initGtag();
  }, []);

  useEffect(() => {
    if (cards.length) {
      cards.forEach(card => DB.put('cards', card));
  
      DB.getAll('cards').then(stored => {
        stored.forEach(s => {
          let c = cards.find(card => card.id === s.id);
          if (!c) {
            console.log(s);
            console.log(cards);
            DB.delete('cards', s.id);
          }
        });
      });
    }

  }, [cards])

  useEffect(() => {
    if (settings) {
      DB.put('settings', settings);
    }
  }, [settings])

  useEffect(() => {
    groups.forEach(group => DB.put('groups', group));

    DB.getAll('groups').then(stored => {
      stored.forEach(s => {
        let g = groups.find(group => group.id === s.id);
        if (!g) DB.delete('groups', s.id);
      });
    });

    if (!groups.map(g => { return g.name }).includes(currentGroup) && currentGroup !== "No Group") {
      setCurrentGroup("All");
    }
  }, [groups, currentGroup])

  useEffect(() => {
    let filteredCards = cards;
    switch (currentGroup) {
      case "All":
        break;
      case "No Group":
        filteredCards = cards.filter(vocab => { return !('groupName' in vocab) });
        break;
      default:
        filteredCards = cards.filter(vocab => { return ('groupName' in vocab && vocab.groupName === currentGroup) });
    }
    filteredCards.sort((a, b) => {
      // if (!a.score) a.score = 0;
      // if (!b.score) b.score = 0;
      return a.id - b.id;
    });
    setFilteredCards(filteredCards);
  }, [currentGroup, cards]);

  function loadData() {
    DB.getAll('cards').then(stored => {
      let cards;

      if (stored && stored.length > 0) {
        cards = stored;
      } else {
        cards = sampleVocabs.getSampleVocabs();
      }

      cards.map((vocab, index) => {
        vocab.id = index + 1;
        return vocab;
      });

      setCards(cards);
    });

    DB.getAll('groups').then(stored => {
      let groups = [];

      if (stored && stored.length > 0) {
        groups = stored;
      }

      groups.map((group, index) => {
        group.id = index + 1;
        return group;
      });

      setGroups(groups);
    });
  }

  function loadSettings() {
    DB.getAll('settings').then(stored => {
      if (stored && stored.length) {
        setSettings(stored[0]);
      } else {
        const defaultSettings = {
          id: 1,
          pushPermission: false,
          flashcardGroup: 'All',
          shuffleCards: true,
          sideToDisplay: true,
          maxFlashcards: 20
        }
        setSettings(defaultSettings);
      }
    });
  }

  function initGtag() {
    gtag('js', new Date());
    gtag('config', 'UA-158639406-1');
  }

  function pageView(page) {
    gtag('config', 'UA-158639406-1', { 'page_path': `pwa/flip/${page}` });
  }

  return (
    <Router>
      <AppBar filteredCards={filteredCards} />
      <Container component="main" maxWidth="xs">
        <div style={containerStyle}>
          <Switch>
            <Route path={`${process.env.REACT_APP_BASE_PATH}/settings`}>
              <Settings pageView={pageView} settings={settings} setSettings={setSettings} groups={groups} />
            </Route>
            <Route path={`${process.env.REACT_APP_BASE_PATH}/group/create`}>
              <NewCardGroup pageView={pageView} groups={groups} setGroups={setGroups} />
            </Route>
            <Route path={`${process.env.REACT_APP_BASE_PATH}/card/create`}>
              <NewCard pageView={pageView} cards={cards} setCards={setCards} groups={groups} />
            </Route>
            <Route path={`${process.env.REACT_APP_BASE_PATH}/flashcard`}>
              {cards.length &&
                <Test pageView={pageView} settings={settings} key={renderKey} rerender={rerender} cards={cards} setCards={setCards} />
              }
            </Route>
            <Route path={`${process.env.REACT_APP_BASE_PATH}/group/:id`}>
              {groups.length &&
                <EditCardGroup pageView={pageView} groups={groups} setGroups={setGroups} cards={cards} setCards={setCards} />
              }
            </Route>
            <Route path={`${process.env.REACT_APP_BASE_PATH}/group`}>
              <GroupList pageView={pageView} cards={cards} filteredCards={filteredCards} setCards={setCards} groups={groups} setGroups={setGroups} />
            </Route>
            <Route path={`${process.env.REACT_APP_BASE_PATH}/card/:id`}>
              {cards.length &&
                <EditCard pageView={pageView} cards={cards} setCards={setCards} groups={groups} />
              }
            </Route>
            <Route path={`${process.env.REACT_APP_BASE_PATH}/about`}>
              <About pageView={pageView} />
            </Route>
            <Route path={`${process.env.REACT_APP_BASE_PATH}/card`}>
              <CardList pageView={pageView} cards={cards} filteredCards={filteredCards} setCards={setCards} groups={groups} currentGroup={currentGroup} setCurrentGroup={setCurrentGroup} />
            </Route>
            <Redirect to={`${process.env.REACT_APP_BASE_PATH}/card`} />
          </Switch>
        </div>
      </Container>
    </Router>
  );
}

export default App;
