import React, { useState, useEffect } from 'react';
import { Redirect, useParams, Link as RouterLink } from "react-router-dom";
import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import Grid from '@material-ui/core/Grid';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import TextData from './TextData';

const useStyles = makeStyles(theme => ({
  outerContainer: {
    position: 'fixed',
    overflowY: 'scroll',
    height: '100%',
    left: 0,
    right: 0,
  },
  heading: {
    padding: theme.spacing(4, 0, 4),
  },
  root: {
    paddingLeft: '16px',
    paddingRight: '16px',
  }
}));


export default function EditCard(props) {
  const [front, setFront] = useState("");
  const [back, setBack] = useState("");
  const [group, setGroup] = useState("No Group");
  const [redirect, setRedirect] = useState(null);
  const [errors, setErrors] = useState({});
  const classes = useStyles();
  let { id } = useParams();

  useEffect(() => {
    if (front) errors.front = "";
    if (back) errors.back = "";
  }, [front, back])

  useEffect(() => {
    const card = props.cards.filter(card => { return card.id === Number(id) })[0];
    setFront(card.front);
    setBack(card.back);
    if (card.groupName) {
      setGroup(card.groupName);
    } else {
      setGroup("No Group");
    }
  }, [props.cards, id])

  useEffect(() => {
    props.pageView('card/edit');
  }, [])

  function editCard() {
    if (validate()) {
      let card = props.cards.filter(card => { return card.id === Number(id) })[0];
      let otherCards = props.cards.filter(card => { return card.id != id });
      console.log(card, otherCards);

      props.setCards([{ ...card, front: front, back: back, groupName: group }, ...otherCards].sort((a, b) => {
        return a.id - b.id;
      }));
      setRedirect(`${process.env.REACT_APP_BASE_PATH}/card`);
    }
  }

  function validate() {
    let err = {};
    let result = true;
    if (!front) {
      result = false
      err.front = "Please input Front";
    }
    if (!back) {
      result = false
      err.back = "Please input Back";
    }
    setErrors(err);
    return result;
  }

  return (
    <div className={classes.outerContainer}>
      <div className={classes.root}>
        {redirect &&
          <Redirect to={redirect} />
        }
        <Typography className={classes.heading} component="h3" variant="h3" align="center" color="textPrimary" gutterBottom>
          {TextData.text.editCard}
        </Typography>
        <Grid container spacing={3}>
          <Grid item xs={12}>
            <TextField
              name="front"
              variant="outlined"
              error={!!errors.front}
              helperText={errors.front}
              id="front"
              label={TextData.text.front}
              onChange={e => setFront(e.target.value)}
              value={front}
              fullWidth
            />
          </Grid>
          <Grid item xs={12}>
            <TextField
              name="back"
              variant="outlined"
              error={!!errors.back}
              helperText={errors.back}
              id="back"
              label={TextData.text.back}
              onChange={e => setBack(e.target.value)}
              value={back}
              fullWidth
            />
          </Grid>
          <Grid item xs={12}>
            <FormControl className={classes.formControl} style={{ width: '100%' }}>
              <InputLabel id="group-select-label">{TextData.text.optionalGroup}</InputLabel>
              <Select
                labelId="group-select-label"
                id="group-select"
                value={group}
                onChange={e => setGroup(e.target.value)}
              >
                <MenuItem value={"No Group"}>{TextData.text.noGroup}</MenuItem>
                {props.groups.map(group => {
                  return <MenuItem key={group.name} value={group.name}>{group.name}</MenuItem>
                })}
              </Select>
            </FormControl>
          </Grid>
          <Grid item xs={12}>
            <Button variant="contained" color="primary" fullWidth onClick={editCard} size="large">{TextData.text.edit}</Button>
          </Grid>
          <Grid item xs={12}>
            <Button variant="outlined" color="default" fullWidth size="large" component={RouterLink} to={`${process.env.REACT_APP_BASE_PATH}/card`}>{TextData.text.cancel}</Button>
          </Grid>
        </Grid>
      </div>
    </div >
  );
}