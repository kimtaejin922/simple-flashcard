let textData = {};

textData.lang = navigator.language;

const getText = function (eng, jp, kr) {
  if (textData.lang.includes('ja')) return jp;
  if (textData.lang.includes('ko')) return kr;
  return eng;
}

textData.text = {
  cardList: getText(
    'Card List',
    'カード一覧',
    '카드 목록'
  ),
  front: getText(
    'Front',
    '表',
    '앞면'
  ),
  back: getText(
    'Back',
    '裏',
    '뒷면'
  ),
  card: getText(
    'Card',
    'カード',
    '카드'
  ),
  showCardList: getText(
    'Show card list',
    'カード一覧',
    '카드 목록'
  ),
  showAsFlashcard: getText(
    'Show as flashcard',
    '単語カード',
    '단어 카드'
  ),
  addNewCard: getText(
    'Add new card',
    'カード追加',
    '카드 추가'
  ),
  cardGroup: getText(
    'Card group',
    'カードグループ',
    '카드 그룹'
  ),
  showGroupList: getText(
    'Show card group list',
    'カードグループ一覧',
    '카드 그룹 목록'
  ),
  addNewCardGroup: getText(
    'Add new card group',
    'カードグループ追加',
    '카드 그룹 추가'
  ),
  editCard: getText(
    'Edit card',
    'カード編集',
    '카드 편집'
  ),
  editCardGroup: getText(
    'Edit card group',
    'カードグループ編集',
    '카드 그룹 편집'
  ),
  add: getText(
    'Add',
    '追加',
    '추가'
  ),
  edit: getText(
    'Edit',
    '編集',
    '편집'
  ),
  cancel: getText(
    'Cancel',
    'キャンセル',
    '취소'
  ),
  all: getText(
    'All',
    '全て',
    '모든 그룹'
  ),
  noGroup: getText(
    'No group',
    '未指定',
    '그룹 없음'
  ),
  optionalGroup: getText(
    '(optional) Card group',
    '（任意）カードグループ',
    '(임의) 카드 그룹'
  ),
  result: getText(
    'Result',
    '結果',
    '(임의) 카드 그룹'
  ),
  numberOfCards: getText(
    'Number of cards',
    'カード数',
    '카드 수'
  ),
  correctAnswerCount: getText(
    'Correct answer count',
    '正解数',
    '정답 수'
  ),
  incorrectAnswerCount: getText(
    'Incorrect answers count',
    '誤解数',
    '오답 수'
  ),
  incorrectAnswers: getText(
    'Incorrect answers',
    '誤解の一覧',
    '오답 노트'
  ),
  retry: getText(
    'Retry',
    'リトライ',
    '재도전'
  ),
  settings: getText(
    'Settings',
    '設定',
    '설정'
  ),
  cardGroupTobeUsed: getText(
    'Card group to be used in flashcard mode',
    '単語カードモードで使うカードグループ',
    '단어카드에 사용될 카드 그룹'
  ),
  shuffleCards: getText(
    'Shuffle cards in flashcard mode',
    '単語カードモードでカードを混ぜる',
    '단어카드 순서 섞기'
  ),
  shuffle: getText(
    'Shuffle cards in flashcard mode',
    'シャッフル',
    '섞기'
  ),
  maximum: getText(
    'Maximum cards displayed in flashcard mode',
    '単語カードモードで表示する最大カード数',
    '단어카드 최대 표시 수'
  ),
  max: getText(
    'Max',
    '最大',
    '최대'
  ),
  settingsSaved: getText(
    'Settings saved successfully.',
    '設定を保存しました。',
    '설정 내용을 보존하였습니다.'
  ),
  yes: getText(
    'Yes',
    'はい',
    '예'
  ),
  no: getText(
    'No',
    'いいえ',
    '아니오'
  ),
  newCardAdded: getText(
    'New Card Added Succesfully.',
    '新しいカードを追加しました。',
    '새로운 카드를 추가하였습니다.'
  ),
  newGroupAdded: getText(
    'New Group Added Succesfully.',
    '新しいカードグループを追加しました。',
    '새로운 카드 그룹을 추가하였습니다.'
  ),
  markCorrect: getText(
    'Marked as correct answer.',
    '正解として記録しました。',
    '정답 처리하였습니다.'
  ),
  markIncorrect: getText(
    'Marked as incorrect answer.',
    '不正解として記録しました。',
    '오답 처리하였습니다.'
  ),
  noCards: getText(
    'No cards in current card group.Please add cards or change card group in settings.',
    'カードグループにカードが存在しません。カードを追加するか、設定でカードグループを変更してください。',
    '카드 그룹에 카드가 없습니다. 카드를 추가하거나 설정에서 카드 그룹을 변경해주세요.'
  ),
  save: getText(
    'Save',
    '保存',
    '보존'
  ),
  sideDisplayed: getText(
    'Side to display initially in flashcard mode',
    '単語カードモードで提示される側',
    '단어카드 모드에서 제시되는 카드 면'
  ),
  side: getText(
    'Side',
    '表・裏',
    '앞뒤'
  ),
  about: getText(
    'About',
    'アプリの紹介',
    '어플 소개'
  ),
  pleaseInputFront: getText(
    'Please input front',
    '表側を入力してください',
    '앞면을 입력해 주세요'
  ),
  pleaseInputBack: getText(
    'Please input back',
    '裏側を入力してください',
    '뒷면을 입력해 주세요'
  ),
  pleaseInputGroupName: getText(
    'Please input group name',
    'カードグループ名を入力してください',
    '카드 그룹명을 입력해 주세요'
  ),
  groupNameAlreadyInUse: getText(
    'Group name already in use',
    'すでに使用中のカードグループ名です。',
    '이미 사용중인 카드 그룹명입니다'
  ),
  recievePushNotification: getText(
    'Recieve Push Notification',
    'プッシュ通知を受ける',
    '푸쉬 알림을 받기'
  ),
}



export default textData;