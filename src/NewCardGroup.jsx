import React, { useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import Grid from '@material-ui/core/Grid';
import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert from '@material-ui/lab/Alert';
import TextData from './TextData';

const useStyles = makeStyles(theme => ({
  outerContainer: {
    position: 'fixed',
    overflowY: 'scroll',
    height: '100%',
    left: 0,
    right: 0,
  },
  heading: {
    padding: theme.spacing(4, 0, 4),
  },
  root: {
    paddingLeft: '16px',
    paddingRight: '16px',
  }
}));

function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}

export default function NewCardGroup(props) {
  const [name, setName] = useState("");
  const [open, setOpen] = useState(false);
  const [errors, setErrors] = useState({});
  const classes = useStyles();

  useEffect(() => {
    props.pageView('group/create');
  }, []);

  useEffect(() => {
    if (name) errors.name = "";
  }, [name])

  function addGroup() {
    if (validate()) {
      props.setGroups([...props.groups, { id: props.groups.length + 1, name: name }]);
      setOpen(true);
      setTimeout(() => setOpen(false), 2000);
      setName("");
    }
  }

  function validate() {
    let err = {}
    let result = true;
    if (!name) {
      result = false
      err.name = TextData.text.pleaseInputGroupName
    }
    const nameArray = ["All", "No Group"];
    props.groups.forEach(group => { nameArray.push(group.name) });
    if (nameArray.includes(name)) {
      result = false
      err.name = TextData.text.groupNameAlreadyInUse;
    }
    setErrors(err);
    return result;
  }

  return (
    <div className={classes.outerContainer}>
      <div className={classes.root}>
        <Typography className={classes.heading} component="h3" variant="h3" align="center" color="textPrimary" gutterBottom>
          {TextData.text.addNewCardGroup}
        </Typography>
        <Grid container spacing={3}>
          <Grid item xs={12}>
            <TextField
              name="name"
              variant="outlined"
              error={!!errors.name}
              helperText={errors.name}
              id="name"
              label="Group Name"
              onChange={e => setName(e.target.value)}
              value={name}
              fullWidth
            />
          </Grid>
          <Grid item xs={12}>
            <Button variant="contained" color="primary" fullWidth onClick={addGroup} size="large">{TextData.text.add}</Button>
          </Grid>
        </Grid>
        <Snackbar open={open}>
          <Alert severity="success" style={{ marginBottom: '30px' }}>
            {TextData.text.newGroupAdded}
          </Alert>
        </Snackbar>
      </div>
    </div>
  );
}