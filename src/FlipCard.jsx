import React, { useState, useEffect, useRef } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import Typography from '@material-ui/core/Typography';

const useStyles = makeStyles({
  card: {
    minWidth: 200,
    maxWidth: 300,
    width: 'min(80vw, 80vh);',
    position: 'relative',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    perspective: '1000px',
    backgroundColor: 'aliceblue',
    backfaceVisibility: 'hidden'
  },
  // cardText: {
  //   top: '50%',
  //   transform: 'translate(0, -50%)',
  //   position: 'sticky'
  // },
  // flipMessage: { 
  //   position: 'sticky', 
  //   top: '100%', 
  //   background: 'lightgray' 
  // }
});

function FlipCard(props) {
  const [isFront, setIsFront] = useState(props.settings.sideToDisplay);
  const classes = useStyles();
  let square = useRef(null);
  let startX = 0;
  let dragDistance = 0;
  let nextPageDistance = 50;
  let isDisabled = false;
  let isClicked = false;

  useEffect(() => {
    if (props.fromLeft) {
      fromLeft();
    } else {
      fromRight();
    }
    square.current.style.display = "block";
    return () => {
      setIsFront(props.settings.sideToDisplay);
    };
  }, [props.index, props.fromLeft])

  const dragStart = e => {
    if (!isDisabled) {
      if (e.type === "mousedown") {
        isClicked = true;
      }
      startX = e.type === "mousedown" ? e.clientX : e.touches[0].clientX;
      square.current.style.removeProperty("transition");
    }
  }

  const dragEnd = e => {
    if (!isDisabled) {
      if (e.type === "mouseup") {
        isClicked = false;
      }
      dragDistance = 0;
      square.current.style.transition = "all 150ms ease-out 0s";
      square.current.style.transform = `translateX(0px)`;
    }
  }

  const dragAction = e => {
    if (isDisabled === false) {
      if (e.type === "mousemove") {
        if (isClicked) {
          dragDistance = e.clientX - startX;
        }
      } else {
        dragDistance = e.touches[0].clientX - startX;
      }

      if (!props.index && dragDistance > 0) {
        dragDistance = 0;
        square.current.style.transform = `translateX(0px)`;
      } else if (props.index === props.cardSet.length - 1 && dragDistance < 0) {
        dragDistance = 0;
      } else {
        square.current.style.transform = `translateX(${dragDistance}px)`;
      }

      if (dragDistance >= nextPageDistance) {
        isDisabled = true;
        slideRight();
      } else if (-dragDistance >= nextPageDistance) {
        isDisabled = true;
        slideLeft();
      }
    }
  }

  const slideLeft = () => {
    square.current.style.transition = "all 150ms ease-out 0s";
    square.current.style.transform = `translateX(-400px)`;
    setTimeout(() => {
      square.current.style.removeProperty("transition");
      square.current.style.transform = `translateX(0px)`;
      square.current.style.visibility = "hidden";
      props.nextCard();
    }, 200);
  }

  const slideRight = () => {
    let offset = window.innerWidth;
    square.current.style.transition = "all 150ms ease-out 0s";
    square.current.style.transform = `translateX(${offset}px)`;
    setTimeout(() => {
      square.current.style.removeProperty("transition");
      square.current.style.transform = `translateX(0px)`;
      square.current.style.visibility = "hidden";
      props.prevCard();
    }, 200);
  }

  function fromLeft() {
    let offset = window.innerWidth;
    square.current.style.visibility = "visible";
    square.current.style.transform = `translateX(-${offset}px)`;
    setTimeout(() => {
      square.current.style.transition = "all 150ms ease-out 0s";
      square.current.style.transform = `translateX(0px)`;
    }, 200);
  }

  function fromRight() {
    let offset = window.innerWidth;
    square.current.style.visibility = "visible";
    square.current.style.transform = `translateX(${offset}px)`;
    setTimeout(() => {
      square.current.style.transition = "all 150ms ease-out 0s";
      square.current.style.transform = `translateX(0px)`;
    }, 200);
  }

  function flip() {
    setTimeout(() => { props.setShowFeedBack(true) }, 200);
    setIsFront(!isFront);
  }

  return (
    <div>
      <div ref={square} className={isFront ? "flip-card-head" : "flip-card-tail"} onTouchStart={e => dragStart(e)} onTouchEnd={e => dragEnd(e)} onTouchMove={e => dragAction(e)} style={{visibility: 'hidden'}}>
        <div className="flip-card-inner">
          <div className="flip-card-front" onClick={() => flip()}>
            <Card key={props.card.front} className={classes.card}>
              <Typography variant="h5" component="h2" className={classes.cardText}>
                {props.card.front}
              </Typography>
              {/* <div className={classes.flipMessage}>Touch to Flip</div> */}
            </Card>
          </div>
          <div className="flip-card-back" onClick={() => flip()}>
            <Card key={props.card.back} className={classes.card}>
              <Typography variant="h5" component="h2" className={classes.cardText}>
                {props.card.back}
              </Typography>
              {/* <div className={classes.flipMessage}>Touch to Flip</div> */}
            </Card>
          </div>
        </div>
      </div>
    </div>
  );
}

export default FlipCard;