import React, { useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import Grid from '@material-ui/core/Grid';
import Snackbar from '@material-ui/core/Snackbar';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import MuiAlert from '@material-ui/lab/Alert';
import TextData from './TextData';

const useStyles = makeStyles(theme => ({
  outerContainer: {
    position: 'fixed',
    overflowY: 'scroll',
    height: '100%',
    left: 0,
    right: 0,
  },
  heading: {
    padding: theme.spacing(4, 0, 4),
  },
  root: {
    paddingLeft: '16px',
    paddingRight: '16px',
  }
}));

function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}

export default function NewCard(props) {
  const [front, setFront] = useState("");
  const [back, setBack] = useState("");
  const [open, setOpen] = useState(false);
  const [group, setGroup] = useState("No Group");
  const [errors, setErrors] = useState({});
  const classes = useStyles();

  useEffect(() => {
    props.pageView('card/create');
  }, []);

  useEffect(() => {
    if (front) errors.front = "";
    if (back) errors.back = "";
  }, [front, back])

  function addCard() {
    if (validate()) {
      props.setCards([...props.cards, { id: props.cards.length + 1, front: front, back: back, groupName: group }]);
      setOpen(true);
      setTimeout(() => setOpen(false), 2000);
      setFront("");
      setBack("");
    }
  }

  function validate() {
    let err = {};
    let result = true;
    if (!front) {
      result = false
      err.front = TextData.text.pleaseInputFront
    }
    if (!back) {
      result = false
      err.back = TextData.text.pleaseInputBack
    }
    setErrors(err);
    return result;
  }

  return (
    <div className={classes.outerContainer}>
      <div className={classes.root}>
        <Typography className={classes.heading} component="h3" variant="h3" align="center" color="textPrimary" gutterBottom>
          {TextData.text.addNewCard}
        </Typography>
        <Grid container spacing={3}>
          <Grid item xs={12}>
            <TextField
              name="front"
              variant="outlined"
              error={!!errors.front}
              helperText={errors.front}
              id="front"
              label={TextData.text.front}
              onChange={e => setFront(e.target.value)}
              value={front}
              fullWidth
            />
          </Grid>
          <Grid item xs={12}>
            <TextField
              name="back"
              variant="outlined"
              error={!!errors.back}
              helperText={errors.back}
              id="back"
              label={TextData.text.back}
              onChange={e => setBack(e.target.value)}
              value={back}
              fullWidth
            />
          </Grid>
          <Grid item xs={12}>
            <FormControl className={classes.formControl} style={{ width: '100%' }}>
              <InputLabel id="group-select-label">{TextData.text.optionalGroup}</InputLabel>
              <Select
                labelId="group-select-label"
                id="group-select"
                value={group}
                onChange={e => setGroup(e.target.value)}
              >
                <MenuItem value={"No Group"}>{TextData.text.noGroup}</MenuItem>
                {props.groups.map(group => {
                  return <MenuItem key={group.name} value={group.name}>{group.name}</MenuItem>
                })}
              </Select>
            </FormControl>
          </Grid>
          <Grid item xs={12}>
            <Button variant="contained" color="primary" fullWidth onClick={addCard} size="large">{TextData.text.add}</Button>
          </Grid>
        </Grid>
        <Snackbar open={open}>
          <Alert severity="success" style={{ marginBottom: '30px' }}>
            {TextData.text.newCardAdded}
          </Alert>
        </Snackbar>
      </div>
    </div>
  );
}