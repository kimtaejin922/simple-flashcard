import React, { useState, useEffect } from 'react';
import {
  Link as RouterLink
} from "react-router-dom";
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Typography from '@material-ui/core/Typography';
import Container from '@material-ui/core/Container';
import Fab from '@material-ui/core/Fab';
import AddIcon from '@material-ui/icons/Add';
import DeleteIcon from '@material-ui/icons/Delete';
import IconButton from '@material-ui/core/IconButton';
import EditIcon from '@material-ui/icons/Edit';
import TextData from './TextData';

const useStyles = makeStyles(theme => ({
  outerContainer: {
    position: 'fixed',
    overflowY: 'scroll',
    height: '100%',
    left: 0,
    right: 0,
  },
  root: {
    width: '100%',
  },
  heading: {
    fontSize: theme.typography.pxToRem(15),
    flexBasis: '33.33%',
    flexShrink: 0,
  },
  secondaryHeading: {
    fontSize: theme.typography.pxToRem(15),
    color: theme.palette.text.secondary,
  },
  heroContent: {
    padding: theme.spacing(4, 0, 4),
    paddingBottom: '100px',
  },
  fab: {
    position: 'fixed',
    bottom: theme.spacing(2),
    right: theme.spacing(2),
  },
  formControl: {
    margin: theme.spacing(1),
    minWidth: 120,
  },
  selectEmpty: {
    marginTop: theme.spacing(2),
  },
}));

export default function GroupList(props) {
  const classes = useStyles();
  const [isDelMode, setIsDelMode] = useState(false);
  const [isEditMode, setIsEditMode] = useState(false);

  useEffect(() => {
    props.pageView('group');
  }, [])

  function deleteGroup(group) {
    if (window.confirm("Delete " + group.name + "?")) {
      props.setGroups(props.groups.filter(g => g.id !== group.id));
    }
    const reGrouped = props.cards.map(card => {
      if ('groupName' in card && card.groupName === group) {
        const { groupName, ...rest } = card;
        return { ...rest }
      }
      return card
    });
    props.setCards(reGrouped);
  }

  return (
    <div className={classes.outerContainer}>
      <div className={classes.heroContent}>
        <Container maxWidth="sm">
          <Typography component="h3" variant="h3" align="center" color="textPrimary" gutterBottom>
            {TextData.text.cardGroup}
          </Typography>
          <div>
            <div style={{ float: "right", top: "25px", position: "relative" }}>
              <IconButton aria-label="add" size="small" component={RouterLink} to={`${process.env.REACT_APP_BASE_PATH}/group/create`}>
                <AddIcon fontSize="small" />
              </IconButton>
              <IconButton style={{ marginLeft: "20px" }} aria-label="edit" size="small" onClick={e => { setIsEditMode(!isEditMode); setIsDelMode(false) }}>
                <EditIcon fontSize="small" color={isEditMode ? "error" : "action"} />
              </IconButton>
              <IconButton style={{ marginLeft: "20px" }} aria-label="delete" size="small" onClick={e => { setIsEditMode(false); setIsDelMode(!isDelMode) }}>
                <DeleteIcon fontSize="small" color={isDelMode ? "error" : "action"} />
              </IconButton>
            </div>
          </div>
          <Table size="small">
            <TableHead>
              <TableRow>
                <TableCell width={isDelMode ? "90%" : "100%"}>
                  Group Name
                </TableCell>
                {(isDelMode || isEditMode) &&
                  <TableCell width="10%">
                  </TableCell>
                }
              </TableRow>
            </TableHead>
            <TableBody>
              {props.groups.map(group => (
                <TableRow key={group.name}>
                  <TableCell size="medium">{group.name}</TableCell>
                  {(isDelMode || isEditMode) &&
                    <TableCell size="medium">
                      {isDelMode &&
                        <IconButton aria-label="delete" size="small" onClick={e => deleteGroup(group)}>
                          <DeleteIcon fontSize="small" />
                        </IconButton>
                      }
                      {isEditMode &&
                        <IconButton aria-label="edit" size="small" component={RouterLink} to={`${process.env.REACT_APP_BASE_PATH}/group/${group.id}`}>
                          <EditIcon fontSize="small" />
                        </IconButton>
                      }
                    </TableCell>
                  }
                </TableRow>
              ))}
            </TableBody>
          </Table>
          <Fab color="primary" aria-label="add" className={classes.fab} component={RouterLink} to={`${process.env.REACT_APP_BASE_PATH}/group/create`}>
            <AddIcon />
          </Fab>
        </Container>
      </div>
    </div>
  );
}
