import React, { useEffect } from 'react';
import { Link as RouterLink } from "react-router-dom";
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Button from '@material-ui/core/Button';
import Container from '@material-ui/core/Container';
import TextData from './TextData';

const useStyles = makeStyles(theme => ({
  outerContainer: {
    position: 'fixed',
    overflowY: 'scroll',
    height: '100%',
    left: 0,
    right: 0,
  },
  heading: {
    padding: theme.spacing(4, 0, 4),
  },
  root: {
    paddingLeft: '16px',
    paddingRight: '16px',
  },
  table: {
    marginTop: '20px',
    marginBottom: '20px'
  },
  buttons: {
    marginTop: '30px',
    paddingBottom: '100px'
  }
}));

export default function Result(props) {
  const classes = useStyles();

  useEffect(() => {
    props.pageView('result');
  }, [])

  return (
    <div className={classes.outerContainer}>
      <div className={classes.root}>
        <Container maxWidth="sm">
          <Typography className={classes.heading} component="h3" variant="h3" align="center" color="textPrimary" gutterBottom>
            {TextData.text.result}
          </Typography>
          <Grid container spacing={3}>
            <Grid item xs={9}>
              <Typography component="h6" variant="h6" align="left" color="textPrimary" gutterBottom>
                {TextData.text.numberOfCards}
              </Typography>
            </Grid>
            <Grid item xs={3}>
              <Typography component="h6" variant="h6" align="center" color="textPrimary" gutterBottom>
                {props.cardSet.length}
              </Typography>
            </Grid>
          </Grid>
          <Grid container spacing={3}>
            <Grid item xs={9}>
              <Typography component="h6" variant="h6" align="left" color="textPrimary" gutterBottom>
                {TextData.text.correctAnswerCount}
              </Typography>
            </Grid>
            <Grid item xs={3}>
              <Typography component="h6" variant="h6" align="center" color="textPrimary" gutterBottom>
                {props.correctList.length}
              </Typography>
            </Grid>
          </Grid>
          <Grid container spacing={3}>
            <Grid item xs={9}>
              <Typography component="h6" variant="h6" align="left" color="textPrimary" gutterBottom>
                {TextData.text.incorrectAnswerCount}
              </Typography>
            </Grid>
            <Grid item xs={3}>
              <Typography component="h6" variant="h6" align="center" color="textPrimary" gutterBottom>
                {props.incorrectList.length}
              </Typography>
            </Grid>
          </Grid>
          {props.incorrectList.length > 0 &&
            <>
              <Grid container spacing={3}>
                <Grid item xs={9}>
                  <Typography component="h6" variant="h6" align="left" color="textPrimary" gutterBottom>
                    {TextData.text.incorrectAnswers}
                  </Typography>
                </Grid>
              </Grid>
              <Table size="small" className={classes.table} >
                <TableHead>
                  <TableRow>
                    <TableCell width="50%">
                      {TextData.text.front}
                    </TableCell>
                    <TableCell width="50%">
                      {TextData.text.back}
                    </TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {props.incorrectList.map(card =>
                    <TableRow key={card.id}>
                      <TableCell size="medium">{card.front}</TableCell>
                      <TableCell size="medium">{card.back}</TableCell>
                    </TableRow>
                  )}
                </TableBody>
              </Table>
            </>
          }
          <Grid className={classes.buttons} container spacing={3}>
            <Grid item xs={6}>
              <Button variant="contained" color="secondary" fullWidth={true} onClick={() => { props.rerender(Math.random()) }}>{TextData.text.retry}</Button>
            </Grid>
            <Grid item xs={6}>
              <Button variant="contained" color="primary" fullWidth={true} component={RouterLink} to={`${process.env.REACT_APP_BASE_PATH}/card`}>{TextData.text.cardList}</Button>
            </Grid>
          </Grid>
        </Container>
      </div>
    </div>
  );
}