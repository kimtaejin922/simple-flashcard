import React, { useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import InputLabel from '@material-ui/core/InputLabel';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import Container from '@material-ui/core/Container';
import Button from '@material-ui/core/Button';
import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert from '@material-ui/lab/Alert';
import TextData from './TextData';
import Switch from '@material-ui/core/Switch';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormGroup from '@material-ui/core/FormGroup';

const useStyles = makeStyles(theme => ({
  outerContainer: {
    position: 'fixed',
    overflowY: 'scroll',
    height: '100%',
    left: 0,
    right: 0,
  },
  heading: {
    padding: theme.spacing(4, 0, 4),
  },
  root: {
    paddingLeft: '16px',
    paddingRight: '16px',
    paddingBottom: '200px'
  },
  container: {
    paddingTop: '10px',
    paddingBottom: '10px'
  },
  formControl: {
    width: '100%',
  },
}));

function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}

export default function Settings(props) {
  const [flashcardGroup, setFlashcardGroup] = useState('All');
  const [shuffleCards, setShuffleCards] = useState(true);
  const [maxFlashcards, setMaxFlashcards] = useState(10);
  const [open, setOpen] = useState(false);
  const [sideToDisplay, setSideToDisplay] = useState(true);
  const [pushPermission, setPushPermission] = useState(false);
  const classes = useStyles();

  useEffect(() => {
    props.pageView('settings');
  }, [])

  useEffect(() => {
    if (props.settings) {
      if (Notification.permission === "granted") setPushPermission(props.settings.pushPermission);
      setFlashcardGroup(props.settings.flashcardGroup);
      setShuffleCards(props.settings.shuffleCards);
      setSideToDisplay(props.settings.sideToDisplay);
      setMaxFlashcards(props.settings.maxFlashcards);
    }
  }, [props.settings])

  function saveSettings() {
    props.setSettings({
      id: 1,
      pushPermission,
      flashcardGroup,
      shuffleCards,
      sideToDisplay,
      maxFlashcards
    });
    setOpen(true);
    setTimeout(() => { setOpen(false) }, 2000);
  }

  function togglePushSwitch() {
    if (!pushPermission) {
      Notification.requestPermission().then(permission => {
        if (permission === "granted") setPushPermission(!pushPermission);
      });
    } else {
      setPushPermission(!pushPermission);
    }
  }

  return (
    <div className={classes.outerContainer}>
      <div className={classes.root}>
        <Typography className={classes.heading} component="h3" variant="h3" align="center" color="textPrimary" gutterBottom>
          {TextData.text.settings}
        </Typography>

        <Container className={classes.container}>
          <Typography component="h6" variant="h6" align="left" color="textPrimary" gutterBottom>
            {TextData.text.recievePushNotification}
          </Typography>
          <FormGroup row>
            <FormControlLabel
              control={
                <Switch
                  checked={pushPermission}
                  onChange={togglePushSwitch}
                  name="checkedB"
                  color="primary"
                />
              }
              // label="Primary"
            />
          </FormGroup>
        </Container>

        <Container className={classes.container}>
          <Typography component="h6" variant="h6" align="left" color="textPrimary" gutterBottom>
            {TextData.text.cardGroupTobeUsed}
          </Typography>
          <FormControl className={classes.formControl}>
            <InputLabel htmlFor="age-native-simple">{TextData.text.cardGroup}</InputLabel>
            <Select
              native
              value={flashcardGroup}
              onChange={e => setFlashcardGroup(e.target.value)}
            >
              <option value="All">{TextData.text.all}</option>
              <option value="No Group">{TextData.text.noGroup}</option>
              {props.groups && props.groups.map(group => {
                return <option key={group.name} value={group.name}>{group.name}</option>
              })}
            </Select>
          </FormControl>
        </Container>

        <Container className={classes.container}>
          <Typography component="h6" variant="h6" align="left" color="textPrimary" gutterBottom>
            {TextData.text.shuffleCards}
          </Typography>
          <FormControl className={classes.formControl}>
            <InputLabel htmlFor="age-native-simple">{TextData.text.shuffle}</InputLabel>
            <Select
              native
              value={shuffleCards}
              onChange={e => setShuffleCards(e.target.value)}
            >
              <option value={true}>{TextData.text.yes}</option>
              <option value={false}>{TextData.text.no}</option>
            </Select>
          </FormControl>
        </Container>

        <Container className={classes.container}>
          <Typography component="h6" variant="h6" align="left" color="textPrimary" gutterBottom>
            {TextData.text.sideDisplayed}
          </Typography>
          <FormControl className={classes.formControl}>
            <InputLabel htmlFor="age-native-simple">{TextData.text.side}</InputLabel>
            <Select
              native
              value={sideToDisplay}
              onChange={e => setSideToDisplay(e.target.value)}
            >
              <option value={true}>{TextData.text.front}</option>
              <option value={false}>{TextData.text.back}</option>
            </Select>
          </FormControl>
        </Container>

        <Container className={classes.container}>
          <Typography component="h6" variant="h6" align="left" color="textPrimary" gutterBottom>
            {TextData.text.maximum}
          </Typography>
          <FormControl className={classes.formControl}>
            <InputLabel htmlFor="age-native-simple">{TextData.text.max}</InputLabel>
            <Select
              native
              value={maxFlashcards}
              onChange={e => setMaxFlashcards(e.target.value)}
            >
              <option value={10}>10</option>
              <option value={20}>20</option>
              <option value={30}>30</option>
              <option value={40}>40</option>
              <option value={50}>50</option>
            </Select>
          </FormControl>
        </Container>
        <Button variant="contained" color="primary" fullWidth onClick={() => { saveSettings() }} size="large" style={{ marginTop: '20px' }}>{TextData.text.save}</Button>
        <Snackbar open={open}>
          <Alert severity="success" style={{ marginBottom: '30px' }}>
            {TextData.text.settingsSaved}
          </Alert>
        </Snackbar>
      </div>
    </div>
  );
}