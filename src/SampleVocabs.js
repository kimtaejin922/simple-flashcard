let sampleVocabs = {}

sampleVocabs.lang = navigator.language;

sampleVocabs.getSampleVocabs = function() {
  if (sampleVocabs.lang.includes('ko')) return sampleVocabs.ko;
  if (sampleVocabs.lang.includes('ja')) return sampleVocabs.ja;
  return sampleVocabs.en;
}

sampleVocabs.ko = [
  {
    "front": "outstanding",
    "back": "뛰어난"
  },
  {
    "front": "promote",
    "back": "촉진시키다"
  },
  {
    "front": "brisk",
    "back": "호황의",
    "groupName": "Economy"
  },
  {
    "front": "industry",
    "back": "산업",
    "groupName": "Economy"
  },
  {
    "front": "prospect",
    "back": "전망",
    "groupName": "Economy"
  },
  {
    "front": "depression",
    "back": "불황",
    "groupName": "Economy"
  },
  {
    "front": "去頭截尾",
    "back": "거두절미: 앞뒤의 잔말을 빼고 요점만 말함.",
    "groupName": "사자성어"
  },
  {
    "front": "群鷄一鶴",
    "back": "군계일학: 평범한 사람들 가운데 뛰어난 한 사람이 섞여 있음.",
    "groupName": "사자성어"
  },
  {
    "front": "苦盡甘來",
    "back": "고진감래: 고생 끝에 즐거움이 온다는 말.",
    "groupName": "사자성어"
  }
]

sampleVocabs.ja = [
  {
    "front": "outstanding",
    "back": "優れた"
  },
  {
    "front": "promote",
    "back": "促す"
  },
  {
    "front": "brisk",
    "back": "好況の",
    "groupName": "Economy"
  },
  {
    "front": "industry",
    "back": "産業",
    "groupName": "Economy"
  },
  {
    "front": "prospect",
    "back": "展望",
    "groupName": "Economy"
  },
  {
    "front": "depression",
    "back": "不況",
    "groupName": "Economy"
  },
  {
    "front": "一期一会",
    "back": "いちごいちえ：一生に一度だけの機会。そのように貴重な出会いのこと。",
    "groupName": "四字熟語"
  },
  {
    "front": "初志貫徹",
    "back": "しょしかんてつ：最初に決めた志を最後まで貫き通すこと。",
    "groupName": "四字熟語"
  },
  {
    "front": "我武者羅",
    "back": "我武者羅：目的だけをみてひたすら打ち込むこと。",
    "groupName": "四字熟語"
  }
]

sampleVocabs.en = [
  {
    "front": "aberration",
    "back": "a state or condition markedly different from the norm"
  },
  {
    "front": "archetype",
    "back": "something that serves as a model"
  },
  {
    "front": "underscore",
    "back": "give extra weight to",
    "groupName": "SAT words"
  },
  {
    "front": "foreshadow",
    "back": "indicate by signs",
    "groupName": "SAT words"
  },
  {
    "front": "rhetorical",
    "back": "relating to using language effectively",
    "groupName": "SAT words"
  },
  {
    "front": "imply",
    "back": "express or state indirectly",
    "groupName": "SAT words"
  },
  {
    "front": "오늘",
    "back": "oneul: today",
    "groupName": "Korean vocabulary"
  },
  {
    "front": "내일",
    "back": "naeil: tomorrow",
    "groupName": "Korean vocabulary"
  },
  {
    "front": "어제",
    "back": "eoje: yesterday",
    "groupName": "Korean vocabulary"
  }
]

export default sampleVocabs;