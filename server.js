const webpush = require('web-push');
const http = require('http');
const schedule = require('node-schedule');

const dataToSend = JSON.stringify({
  "notification": {
    "title": "flip",
    "body": "push notification from flip"
  }
});

const vapidKeys = {
  publicKey:
    'BEl62iUYgUivxIkv69yViEuiBIa-Ib9-SkvMeAtA3LFgDzkrxZJjSgSnfckjBJuBkr3qBUYIHBQFLXYp5Nksh8U',
  privateKey: 'UUxI4O8-FbRouAevSmBQ6o18hgE4nSG3qwvJTfKc-ls'
};

webpush.setVapidDetails(
  'mailto:oldmanstudio922@gmail.com',
  vapidKeys.publicKey,
  vapidKeys.privateKey
);

let subscriptions = [];

var server = http.createServer(function (request, response) {
  var url = request.url;

  response.writeHead(200, {
    'Access-Control-Allow-Origin': '*',
    'Access-Control-Allow-Headers': '"Origin, Content-Type, Accept',
    'Access-Control-Allow-Methods': 'POST, OPTIONS',
  });

  if (url === '/push-subscription') {
    let body = '';
    request.on('data', chunk => {
      body += chunk;
      console.log(`Data chunk available: ${chunk}`)
    })
    request.on('end', () => {
      console.log(body);
      subscription = JSON.parse(body);
      if (subscriptions.every(s => s.endpoint !== subscription.endpoint)) subscriptions.push(subscription);
      if (subscriptions.length > 1000) subscriptions.pop();
    })
  }

  if (url === "/activate-push") {
    subscriptions.forEach(subscription => webpush.sendNotification(subscription, dataToSend));    
  }

  response.write(JSON.stringify({ result: "success" }));
  response.end();
}).listen(8080, function () {
  console.log((new Date()) + "Server is listening on port 8080")
});

schedule.scheduleJob('0 52 21 * * *', function(){
  console.log('Sending Notifications!');
  subscriptions.forEach(subscription => webpush.sendNotification(subscription, dataToSend));  
});